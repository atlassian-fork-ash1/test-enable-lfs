# @atlaskit/adf-schema

## 1.3.0
- [minor] [cbcac2e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/cbcac2e):

  - Promote smartcard nodes to full schema

## 1.2.0
- [minor] [5b11b69](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/5b11b69):

  - Allow mixed of cell types in a table row

## 1.1.0
- [minor] [b9f8a8f](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b9f8a8f):

  - Adding alignment options to media

## 1.0.1
- [patch] [d7bfd60](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d7bfd60):

  - Rengenerate JSON schema after moving packages

## 1.0.0
- [major] [1205725](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1205725):

  - Move schema to its own package
